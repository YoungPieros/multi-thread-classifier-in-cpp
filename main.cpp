#include <iostream>
#include <fstream>
#include <string>
#include <cmath>
#include <vector>
#include <sstream>
#include <sys/types.h>
#include <sys/wait.h>
#include <sys/stat.h>
#include <unistd.h>
#include <fcntl.h>
#include <stdlib.h>
#include <stdio.h>


#define DATASET_FILE "dataset.csv"
#define LABELS_FILE "labels.csv"
#define CLASSIFIER "classifier"
#define NAMED_PIPE "namedPipe"

using namespace std;


int findClassifierNumbers (string weightVectorFolder) {
    int fileCounter = 0;
    for (; ; fileCounter++) {
        string fileName = weightVectorFolder + "/" + CLASSIFIER + "_" + to_string(fileCounter) + ".csv";
        if (access(fileName.c_str(), 0) == -1)
            break;
    }
    return fileCounter;
}

vector <double> readLabels (string file) {
    vector <double> data;
    ifstream datasetFile(file);
    string strNumber;
    getline(datasetFile, strNumber, '\n');
    while (getline(datasetFile, strNumber, '\n'))
        data.push_back(stoi(strNumber));
    return data;
}

vector <string> declareNamedPipes (int pipeNumbers) {
    vector <string> pipes(pipeNumbers);
    string namedPipe = NAMED_PIPE;
    for (int i = 0; i < pipeNumbers; i++) {
        pipes[i] = namedPipe + "_" + to_string(i) + ".txt";
        int fd;
        if (fd = open(pipes[i].c_str(), O_CREAT | O_RDWR, S_IRWXU) < 0)
            perror ("open()");;
        close(fd);
        mkfifo (pipes[i].c_str(), 0666);
    }
    return pipes;
}

vector <int*> declareUnnamedPipes (int pipeNumbers) {
    vector <int*> pipes(pipeNumbers, 0);
    for (int i = 0; i < pipeNumbers; i++) {
        int *newPipe = new int(2);
        if (pipe(newPipe) < 0) {
            i = i - 1;
            perror("new pipe");
        }
        else
            pipes[i] = newPipe;
    }
    return pipes;
}

void writeDataOnPipes (const vector <int*>& pipes, string folderPath) {
    for (int i = 0; i < pipes.size(); i++) {
        string classifier = folderPath + "/" + string(CLASSIFIER) + "_" + to_string(i) + ".csv";
        if (write(pipes[i][1], classifier.c_str(), classifier.size()) < 0)
            perror("writing on pipe");
    }
}

void createLinearClassifiers (vector <int*> unnamedPipes, vector <string> namedPipes, string validationFolder, int classifierNumber) {
    for (int i = 0; i < classifierNumber; i++) {
        pid_t pid = fork();
        if (pid < 0)
            perror ("create new child with fork");
        else if (pid == 0) { /* child pid */
            string unnamedPipe = to_string(unnamedPipes[i][0]);
            string datasetPath = validationFolder + '/' + DATASET_FILE;
            string namedPipe = namedPipes[i];
            if(execl ("./LinearClassifier.out", unnamedPipe.c_str(), datasetPath.c_str(), namedPipe.c_str(), (char *)0) < 0)
                perror ("create exec is failed");
        }
    }
}

pid_t createVoter (const char* voterPipe, int classifierNumbers) {
    string classifierNumbersStr = to_string(classifierNumbers);
    pid_t pid = fork();
    int fd;
    if (fd = open(voterPipe, O_CREAT | O_RDWR, S_IRWXU) < 0)
        perror ("open()");
    close (fd);
    mkfifo(voterPipe, 0666);
    if (pid < 0)
        perror ("create new child with fork");
    else if (pid == 0) { /* child pid */
        if(execl ("./Voter.out", voterPipe, NAMED_PIPE, classifierNumbersStr.c_str(), (char *)0) < 0)
            perror ("create exec is failed");
    }
    return pid;
}

double calculateAccuracy (const char* votePipe, string labelsFile) {
    vector <double> mainLabels = readLabels(labelsFile);
    vector <double> classifierResult = readLabels(votePipe);
    int correctLabels = 0;
    for (int i = 0; i < mainLabels.size(); i++)
        if (mainLabels[i] == classifierResult[i])
            correctLabels++;
    return correctLabels * 100.0 / (mainLabels.size() * 1.0);
}

void freePipes(vector <int*> unnamedPipes) {
    for (int i = 0; i < unnamedPipes.size(); i++) {
        close(unnamedPipes[i][0]);
        close(unnamedPipes[i][1]);
        free(unnamedPipes[i]);
    }
}

double findAccuracy (string validationFolder, string weightVectorFolder) {
    const char *voterPipe = "voterPipe";
    int classifierNumbers = findClassifierNumbers (weightVectorFolder);
    vector <int*> unnamedPipes = declareUnnamedPipes (classifierNumbers);
    vector <string> namedPipes = declareNamedPipes(classifierNumbers);
    writeDataOnPipes(unnamedPipes, weightVectorFolder);
    createLinearClassifiers (unnamedPipes, namedPipes, validationFolder, classifierNumbers);
    while (wait(NULL) > 0);
    pid_t voterPid = createVoter (voterPipe, classifierNumbers);
    while (wait(NULL) > 0);
    double accuracy = calculateAccuracy(voterPipe, validationFolder + '/' + LABELS_FILE);
    freePipes(unnamedPipes);
    return accuracy;
}

void roundAccuracy (double& accuracy) {
    accuracy = (floor(accuracy * 100) / 100);
}

int main(int argc, char **argv) {
    string validationFolderPath, weighVectorsFolderPath;
    if (argc != 3) {
        cout << "wrong command, try ./EnsembleClassifier.out Assets/validation Assets/weight_vectors" << endl;
        exit (0);
    }
    validationFolderPath = argv[1];
    weighVectorsFolderPath = argv[2];
    double accuracy = findAccuracy (validationFolderPath, weighVectorsFolderPath);
    roundAccuracy(accuracy);
    cout << "Accuracy: " << accuracy << endl;
    return 0;
}





