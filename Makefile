all:  EnsembleClassifier.out

EnsembleClassifier.out: Voter.out LinearClassifier.out main.cpp
	g++ -std=c++11 main.cpp -o EnsembleClassifier.out
	rm *.txt
	rm voterPipe

Voter.out: Voter.cpp
	g++ -std=c++11 Voter.cpp -o Voter.out


LinearClassifier.out: LinearClassifier.cpp
	g++ -std=c++11 LinearClassifier.cpp -o LinearClassifier.out


clean:
	rm EnsembleClassifier.out LinearClassifier.out Voter.out

