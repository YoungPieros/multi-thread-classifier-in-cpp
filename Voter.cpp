#include <iostream>
#include <fstream>
#include <string>
#include <cmath>
#include <vector>
#include <sstream>
#include <sys/types.h>
#include <sys/wait.h>
#include <sys/stat.h>
#include <unistd.h>
#include <fcntl.h>
#include <stdlib.h>
#include <stdio.h>
#include <map>

using namespace std;

vector <int> extractOneClassifierResult (int number, string namedPipe) {
    vector <int> result;
    string value, filePath = namedPipe + "_" + to_string(number) + ".txt";
    ifstream file(filePath);
    getline (file, value);
    while (getline(file, value))
        result.push_back(stod(value));
    return result;
}

vector < vector<int> > extractAllClassifierResult (string namedpipe, int maxClassifiers){
    vector < vector<int> > results;
    for (int i = 0; i < maxClassifiers; i++) {
        results.push_back(extractOneClassifierResult((i), namedpipe));
    }
    return results;
}


int chooseClassOfSample (vector < vector<int> > &classifierResults, int sampleNumber, int classifierNumber) {
    map <int, int> classResult;
    map <int, int>::iterator it;
    int finalResult, maxValue;
    for (int i = 0; i < classifierResults.size(); i++)
        if (classResult.count(classifierResults[i][sampleNumber]) == 0)
            classResult[classifierResults[i][sampleNumber]] = 1;
        else
            classResult[classifierResults[i][sampleNumber]]++;
    it = classResult.begin();
    finalResult = classResult.begin()->first;
    maxValue = classResult.begin()->second;
    for (it = classResult.begin(); it != classResult.end(); it++) {
        pair <int, int> tempVote = (pair <int, int>)*it;
        if (tempVote.second > maxValue || (tempVote.second == maxValue && finalResult > tempVote.first)) {
            maxValue = tempVote.second;
            finalResult = tempVote.first;
        }
    }
    return finalResult;
}

vector <int> chooseClassOfAllSamples (vector < vector<int> > &classifierResult, int classifiersNumber) {
    int sampleNumbers = classifierResult[0].size();
    vector <int> result(sampleNumbers);
    for (int i = 0; i < sampleNumbers; i++)
        result[i] = chooseClassOfSample(classifierResult, i, classifiersNumber);
    return result;
}

void writeResultOnPipe (vector <int> &result, string votePipe) {
    int fd;
    if (fd = open(votePipe.c_str(), O_CREAT | O_RDWR, S_IRWXU) < 0)
        perror ("open()");
    close (fd);
    mkfifo (votePipe.c_str(), 0666);
    ofstream voteFile (votePipe);
    voteFile << "ClassNumber\n";
    for (int i = 0; i < result.size(); i++)
        voteFile << to_string(result[i]) + "\n";
    voteFile.close();
}

int main (int argc, char **argv) {
    string voterPipe = argv[0];
    string namedPipe = argv[1];
    int classifiersNumber = stoi(argv[2]);
    vector < vector<int> > classifiersResult = extractAllClassifierResult(namedPipe, classifiersNumber);
    vector <int> finalResult = chooseClassOfAllSamples(classifiersResult, classifiersNumber);
    writeResultOnPipe(finalResult, voterPipe);
    exit(0);
}



