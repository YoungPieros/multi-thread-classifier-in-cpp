#include <iostream>
#include <fstream>
#include <string>
#include <cmath>
#include <vector>
#include <sstream>
#include <sys/types.h>
#include <sys/wait.h>
#include <sys/stat.h>
#include <unistd.h>
#include <fcntl.h>
#include <stdlib.h>
#include <stdio.h>

using namespace std;

vector < vector<double> > readDataSetValues (string filePath, int parameterNumber) {
    ifstream file(filePath);
    vector < vector<double> > data;
    string dataLine, value;
    vector <double> sampleValues(parameterNumber);
    getline(file, dataLine);
    while (getline(file,dataLine)) {
        stringstream lineStream (dataLine);
        for (int i = 0; i < parameterNumber; i++) {
            getline(lineStream, value, ',');
            sampleValues[i] = stod(value);
        }
        data.push_back(sampleValues);
    }
    return data;
}

string extractClassifierFromPipe (int fd) {
    char buffer[1024];
    string classifier;
    read(fd, buffer, sizeof(buffer));
    classifier = buffer;
    return classifier;
}

int setClassSample (const vector <double> &sample,const vector < vector<double> > &classifier) {
    int classNumber = 0;
    double maxValue = sample[0] * classifier[0][0] + sample[1] * classifier[0][1] + classifier[0][2];
    for (int i = 1; i < classifier.size(); i++) {
        double newValue = sample[0] * classifier[i][0] + sample[1] * classifier[i][1] + classifier[i][2];
        if (newValue > maxValue) {
            classNumber = i;
            maxValue = newValue;
        }
    }
    return classNumber;
}

vector <int> calculateResult (const vector < vector<double> > &dataset, const vector < vector<double> > &classifierParams) {
    vector <int> result(dataset.size());
    for (int i = 0; i < dataset.size(); i++)
        result[i] = setClassSample(dataset[i], classifierParams);
    return result;
}

void writeDataOnPipes(string namedPipe, vector <int> result) {
    int fd;
    if (fd = open(namedPipe.c_str(), O_CREAT | O_RDWR, S_IRWXU) < 0)
        perror ("open()");
    close(fd);
    mkfifo(namedPipe.c_str(), 0666);
    ofstream file (namedPipe);
    file << "ClassNumber\n";
    for (int i = 0; i < result.size(); i++)
        file << to_string(result[i]) + "\n";
    file.close();
}


int main (int argc, char **argv) {
    int fdPipe = atoi(argv[0]);
    string datasetFile = argv[1];
    string classifierFile = extractClassifierFromPipe(fdPipe);
    string namedPipe = argv[2];
    vector < vector<double> > dataset = readDataSetValues(datasetFile, 2);
    vector < vector<double> > classifierParams = readDataSetValues(classifierFile, 3);
    vector <int> result = calculateResult(dataset, classifierParams);
    writeDataOnPipes(namedPipe, result);
    exit(0);
}
